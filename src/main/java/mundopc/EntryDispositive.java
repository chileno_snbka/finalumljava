
package mundopc;

public class EntryDispositive {
   private String entryType;
   private String brand;

    public EntryDispositive(String entryType, String brand) {
        this.entryType = entryType;
        this.brand = brand;
    }

    @Override
    public String toString() {
        return "EntryDispositive{" + "entryType=" + entryType + ", brand=" + brand + '}';
    }

    public String getEntryType() {
        return entryType;
    }

    public void setEntryType(String entryType) {
        this.entryType = entryType;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }
    
    
}
