
package mundopc;

public class Keyboard extends EntryDispositive{
    private final int idKeyboard;
    private static int countKeyboard;

    public Keyboard(String entryType, String brand) {
        super(entryType, brand);
        this.idKeyboard = ++countKeyboard;
    }

    @Override
    public String toString() {
        return "Keyboard{" + "idKeyboard=" + idKeyboard + '}' + super.toString();
    }
    
    
}
