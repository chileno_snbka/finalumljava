package mundopc;

public class Monitor {

    private final int idMonitor;
    private String brand;
    private double size;
    private static int countMonitor;

    private Monitor() {
        this.idMonitor = ++countMonitor;

    }

    public Monitor(String brand, double size) {
        this();
        this.brand = brand;
        this.size = size;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Double getSize() {
        return size;
    }

    public void setSize(Double size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return "Monitor{" + "idMonitor=" + idMonitor + ", brand=" + brand + ", size=" + size + '}';
    }

    

}
