package mundopc;

public class Mouse extends EntryDispositive {

    private final int idMouse;
    private static int countMouse;

    public Mouse(String entryType, String brand) {
        super(entryType, brand);
        this.idMouse = ++countMouse;
    }

    @Override
    public String toString() {
        return "Mouse{" + "idMouse=" + idMouse + '}' + super.toString();
    }
}
