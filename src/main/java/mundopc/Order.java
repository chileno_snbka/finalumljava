
package mundopc;

import java.util.Date;

public class Order {
    private Date date;
    private final int idOrder;
    private final Computer computers[];
    private static int countOrder;
    private int countComputer;
    private static final int MAX_COMPUTERS = 10;

    public Order() {
        this.idOrder = ++countOrder;
        computers = new Computer[MAX_COMPUTERS];
    }
    
    public void addComputer(Computer computer){
        if(this.countComputer < MAX_COMPUTERS){
         computers[this.countComputer++] = computer;
        }
        else{
            System.out.println("Se ha superado el máximo de productos: " + MAX_COMPUTERS);
        }
    }
    
    public void showOrder(){
        System.out.println("Id orden: " + this.idOrder);
        System.out.println("Fecha orden: " + this.getDate());
        System.out.println("Computadores de la orden: ");
        for(int i=0; i<this.countComputer; i++){
            System.out.println(computers[i]);
        }
    }

    private Date getDate() {
         date = new Date();
        return date;
    }
    
}
