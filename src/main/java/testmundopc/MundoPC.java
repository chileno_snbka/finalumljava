
package testmundopc;

import mundopc.*;

public class MundoPC {
    public static void main(String[] args) {
        
        //creacion de computadora Toshiba
        Monitor monitorToshiba = new Monitor("Toshibsa", 13);
        Keyboard tecladoToshi = new Keyboard("bluetooth", "Toshiba");
        Mouse ratonToshi = new Mouse("bluetooth", "Toshiba");
        Computer compuToshiba = new Computer("Computadora Toshiba", monitorToshiba, tecladoToshi, ratonToshi);

        //creacion de computadora dell
        Monitor monitorDell = new Monitor("Dell", 15);
        Keyboard tecladoDell = new Keyboard("usb", "Dell");
        Mouse ratonDell = new Mouse("usb", "Dell");
        Computer compuDell = new Computer("Computadora Dell", monitorDell, tecladoDell, ratonDell);

        //creacion de computadora armada
        Computer compuArmada = new Computer("Computadora Armada", monitorDell, tecladoToshi, ratonToshi);

        //Agregamos las computadoras a la orden 
        Order orden1 = new Order();
        orden1.addComputer(compuToshiba);
        orden1.addComputer(compuDell);
        orden1.addComputer(compuArmada);
        //Imprimimos la orden
        orden1.showOrder();
        
        //Agregamos una segunda orden
        Order orden2 = new Order();
        orden2.addComputer(compuArmada);
        orden2.addComputer(compuDell);
        System.out.println("");
        orden2.showOrder();
    }
}
